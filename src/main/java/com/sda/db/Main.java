package com.sda.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Cat c = new Cat("Rys", 2000, "Brown");
        Cat c2 = new Cat("Kotlin", 1999, "Black");

        CatManager catManager = new CatManager();
        catManager.createCat(sessionFactory.openSession(), c);
        catManager.createCat(sessionFactory.openSession(), c2);
    }
}
